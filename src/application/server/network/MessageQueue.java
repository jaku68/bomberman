package application.server.network;

import java.util.LinkedList;
import java.util.Queue;

public class MessageQueue {
	private Queue<PlayerMessage> queue;
	
	public MessageQueue() {
		queue = new LinkedList<PlayerMessage>();
	}
	
	public synchronized PlayerMessage get() {
		if (queue.isEmpty()) {
			try {
				wait(); // block the listener thread until a new message arrives
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		return queue.poll();
	}
	
	public synchronized void add(PlayerMessage message) {
		queue.add(message);
		notify(); // awake our listener thread when a new message arrives
	}
}
