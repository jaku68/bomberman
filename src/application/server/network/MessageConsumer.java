package application.server.network;

import network.Message;
import network.server.ServerApplicationInterface;

public class MessageConsumer implements ServerApplicationInterface {
	private MessageQueue messageQueue;
	
	public MessageConsumer(MessageQueue messageQueue) {
		this.messageQueue = messageQueue;
	}
	
	@Override
	public void handleMessage(Message message, String connectionId) {
		PlayerMessage playerMessage = new PlayerMessage(connectionId, message);
		messageQueue.add(playerMessage);
	}
}
