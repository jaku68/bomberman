package application.server.network;

import network.Message;

public class PlayerMessage {
	private String connectionId;
	private Message message;
	
	public PlayerMessage(String connectionId, Message message) {
		this.connectionId = connectionId;
		this.message = message;
	}
	
	public String getConnectionId() {
		return connectionId;
	}
	
	public Message getMessage() {
		return message;
	}
}
