package application.server.businesslogic;

import application.server.model.PlayerManager;
import application.server.model.BombManager;
import application.server.model.Map;
import application.server.model.Player;
import network.server.Server;
import protocol.Direction;
import protocol.client2server.MovePlayer;

public class MovePlayerController extends ControllerBase<MovePlayer> {
	private Map map;
	private PlayerManager playerManager;
	private BombManager bombManager;
	
	public MovePlayerController(Server server, Map map, PlayerManager playerManager, BombManager bombManager) {
		super(server, MovePlayer.class);
		this.map = map;
		this.playerManager = playerManager;
		this.bombManager = bombManager;
	}

	@Override
	void handleMessage(MovePlayer message, String connectionId) {
		if (!playerManager.isNumberOfPlayersComplete()) {
			return;
		}
		
		Player player = playerManager.getPlayerOrThrow(message.getPlayerName());
		Direction direction = message.getDirection();
		
		if (player.move(direction, map, playerManager, bombManager)) {
			server.broadcast(player.createPlayerMovedMessage(direction));
		}
	}
}
