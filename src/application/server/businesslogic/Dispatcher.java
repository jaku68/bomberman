package application.server.businesslogic;

import application.server.network.MessageQueue;
import application.server.network.PlayerMessage;
import network.Message;

public class Dispatcher extends Thread {
	private MessageQueue messageQueue;
	private ControllerFactory controllerFactory;
	private volatile boolean running = true;
	
	public Dispatcher(MessageQueue messageQueue, ControllerFactory controllerFactory) {
		this.messageQueue = messageQueue;
		this.controllerFactory = controllerFactory;
	}
	
	@Override
	public void run() {
		while (running) {
			PlayerMessage playerMessage = messageQueue.get();
			
			String connectionId = playerMessage.getConnectionId();
			Message message = playerMessage.getMessage();
			
			Controller controller = controllerFactory.createController(message);
			if (controller != null) {
				controller.execute(message, connectionId);
			}
		}
	}
	
	public void stopDispatcher() {
		running = false;
	}
}
