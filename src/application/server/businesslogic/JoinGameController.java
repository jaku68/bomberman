package application.server.businesslogic;

import application.server.model.PlayerManager;
import application.server.model.Map;
import application.server.model.Player;
import network.server.Server;
import protocol.client2server.JoinGame;
import protocol.server2client.ErrorMessage;

public class JoinGameController extends ControllerBase<JoinGame> {
	private Map map;
	private PlayerManager playerManager;
	
	public JoinGameController(Server server, Map map, PlayerManager playerManager) {
		super(server, JoinGame.class);
		this.map = map;
		this.playerManager = playerManager;
	}

	@Override
	public void handleMessage(JoinGame message, String connectionId) {
		if (playerManager.isNumberOfPlayersComplete()) {
			sendErrorMessage("The game is already running...", connectionId);
			return;
		}
		
		if (!playerManager.isPlayerUnique(message.getPlayerName())) {
			sendErrorMessage("There is already a player with the same name...", connectionId);
			return;
		}
		
		Player player = playerManager.addPlayer(message.getPlayerName(), map);
		server.broadcast(player.createPlayerJoinedMessage());
		
		if (playerManager.isNumberOfPlayersComplete()) {
			server.broadcast(map.createStartGameMessage());
		}
	}
	
	private void sendErrorMessage(String message, String connectionId) {
		server.send(new ErrorMessage(message), connectionId);
	}
}
