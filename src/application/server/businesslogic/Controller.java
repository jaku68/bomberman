package application.server.businesslogic;

import network.Message;

public interface Controller {
	public void execute(Message message, String connectionId);
}
