package application.server.businesslogic;


import application.server.model.BombManager;
import application.server.model.Map;
import application.server.model.PlayerManager;
import network.Message;
import network.server.Server;
import protocol.client2server.DropBomb;
import protocol.client2server.JoinGame;
import protocol.client2server.MovePlayer;

public class ControllerFactory {
	private Server server;
	private Map map;
	private PlayerManager playerManager;
	private BombManager bombManager;
	
	public ControllerFactory(Server server, Map map, PlayerManager playerManager, BombManager bombManager) {
		this.server = server;
		this.map = map;
		this.playerManager = playerManager;
		this.bombManager = bombManager;
	}
	
	public Controller createController(Message message) {
		if (message instanceof JoinGame) {
			return new JoinGameController(server, map, playerManager);
		}
		
		if (message instanceof MovePlayer) {
			return new MovePlayerController(server, map, playerManager, bombManager);
		}
		
		if (message instanceof DropBomb) {
			return new DropBombController(server, map, playerManager, bombManager);
		}
		
		return null;
	}
}
