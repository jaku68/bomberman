package application.server.businesslogic;

import java.util.ArrayList;
import java.util.List;

import application.server.model.PlayerManager;
import application.server.labyrinth.MapFactory;
import application.server.model.BombManager;
import application.server.model.Map;
import application.server.model.Position;
import application.server.model.Wall;
import application.server.network.MessageConsumer;
import application.server.network.MessageQueue;
import application.server.network.ServerStub;
import network.server.Server;

public class Main {
	public static void main(String[] args) {
		new Main();
	}
	
	private Main() {
		// Network
		MessageQueue messageQueue = new MessageQueue();
		MessageConsumer messageConsumer = new MessageConsumer(messageQueue);
		Server server = new ServerStub(messageConsumer);
		
		// Game Preparation
		MapFactory mapFactory = new MapFactory();
		Map map = mapFactory.create("C:\\git\\M326_Bomberman_2017\\src\\editor\\map.json");
		PlayerManager playerManager = new PlayerManager();
		BombManager bombManager = new BombManager();
		
		// Message Dispatching
		ControllerFactory controllerFactory = new ControllerFactory(server, map, playerManager, bombManager);
		Dispatcher dispatcher = new Dispatcher(messageQueue, controllerFactory);
		dispatcher.start();
	}
}
