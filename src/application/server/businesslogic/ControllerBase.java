package application.server.businesslogic;

import network.Message;
import network.server.Server;

public abstract class ControllerBase<TMessage extends Message> implements Controller {
	static Object lockPad = new Object();
	Server server;
	private Class<TMessage> type;
	
	public ControllerBase(Server server, Class<TMessage> type) {
		this.server = server;
		this.type = type;
	}
	
	public void execute(Message message, String connectionId) {
		synchronized (lockPad) {
			handleMessage(type.cast(message), connectionId);
		}
	}
	
	abstract void handleMessage(TMessage message, String connectionId);
}
