package application.server.businesslogic;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import application.server.model.Bomb;
import application.server.model.BombManager;
import application.server.model.Map;
import application.server.model.Player;
import application.server.model.PlayerManager;
import application.server.model.Position;
import network.server.Server;
import protocol.client2server.DropBomb;

public class DropBombController extends ControllerBase<DropBomb> {
	private Map map;
	private PlayerManager playerManager;
	private BombManager bombManager;
	
	public DropBombController(Server server, Map map, PlayerManager playerManager, BombManager bombManager) {
		super(server, DropBomb.class);
		this.map = map;
		this.playerManager = playerManager;
		this.bombManager = bombManager;
	}

	@Override
	void handleMessage(DropBomb message, String connectionId) {
		if (!playerManager.isNumberOfPlayersComplete()) {
			return;
		}
		
		String playerName = message.getPlayerName();
		Position position = new Position(message.getPositionX(), message.getPositionY());
		
		Bomb bomb = bombManager.dropBomb(playerName, position);
		if (bomb == null) {
			return;
		}
		
		server.broadcast(bomb.createBombDroppedMessage());
		
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				synchronized(lockPad) {
					server.broadcast(bomb.createBombExplodedMessage());
					
					List<Player> killedPlayers = bomb.explodeAndGetKilledPlayers(map, playerManager, bombManager);
					for (Player killedPlayer : killedPlayers) {
						server.broadcast(killedPlayer.createPlayerHitMessage());
					}
					
					server.broadcast(map.createUpdateMessage());
					
					if (playerManager.isGameOver()) {
						server.broadcast(playerManager.createGameOverMessage());
					}
				}
			}
		}, 2000);
	}
}
