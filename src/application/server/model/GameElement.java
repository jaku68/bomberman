package application.server.model;

public abstract class GameElement {
	Position position;
	
	public GameElement(Position position) {
		this.position = position;
	}
	
	public boolean isAtPosition(Position position) {
		return this.position.equals(position);
	}
}
