package application.server.model;

import java.util.ArrayList;
import java.util.List;

public class BombManager {
	private List<Bomb> bombs = new ArrayList<Bomb>();
	
	public Bomb dropBomb(String playerName, Position position) {
		if (existsAnyBombAtPosition(position)) {
			return null;
		}
		
		Bomb bomb = new Bomb(playerName, position);
		bombs.add(bomb);
		
		return bomb;
	}
	
	public void removeBomb(Bomb bomb) {
		bombs.remove(bomb);
	}
	
	public boolean existsAnyBombAtPosition(Position position) {
		return bombs.stream()
			.anyMatch(bomb -> bomb.isAtPosition(position));
	}
}
