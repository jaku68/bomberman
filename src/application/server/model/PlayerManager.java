package application.server.model;

import java.util.ArrayList;
import java.util.List;

import protocol.server2client.GameOver;

public class PlayerManager {
	private final int NUMBER_OF_PLAYERS = 4;
	private List<Player> players = new ArrayList<Player>();
	
	public Player addPlayer(String playerName, Map map) {
		Position spawnPoint = map.getSpawnPoint(players.size());
		
		Player player = new Player(spawnPoint, playerName);
		players.add(player);
		
		return player;
	}
	
	public Player getPlayerOrThrow(String playerName) {
		return players.stream()
			.filter(player -> player.hasName(playerName))
			.findFirst().orElseThrow();
	}
	
	public Player killPlayerAtPosition(Position position) {
		Player playerToKill = players.stream()
			.filter(player -> player.isAlive() && player.isAtPosition(position))
			.findFirst().orElse(null);
		
		if (playerToKill != null) {
			playerToKill.kill();
			
			return playerToKill;
		}
		
		return null;
	}
	
	public boolean isPlayerUnique(String playerName) {
		return !players.stream()
			.anyMatch(player -> player.hasName(playerName));
	}
	
	public boolean isNumberOfPlayersComplete() {
		return players.size() >= NUMBER_OF_PLAYERS;
	}
	
	public boolean isGameOver() {
		long numberOfLivingPlayers = players.stream()
			.filter(player -> player.isAlive())
			.count();
		
		return numberOfLivingPlayers <= 1;
	}
	
	public boolean existsAnyPlayerAtPosition(Position position) {
		return players.stream()
			.anyMatch(player -> player.isAlive() && player.isAtPosition(position));
	}
	
	public GameOver createGameOverMessage() {
		Player livingPlayer = players.stream()
			.filter(player -> player.isAlive())
			.findFirst().orElse(null);
		
		if (livingPlayer != null) {
			return livingPlayer.createGameOverMessage(getHighScoreList());
		}
		
		Player playerWithMaxScore = players.stream()
			.max((a, b) -> Integer.compare(a.getScore(), b.getScore()))
			.orElseThrow();
			
		return playerWithMaxScore.createGameOverMessage(getHighScoreList());
	}
	
	private String[] getHighScoreList() {
		return players.stream()
			.map(player -> player.createHighScoreListItem())
			.toArray(String[]::new);
	}
}
