package application.server.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import protocol.Direction;
import protocol.server2client.BombDropped;
import protocol.server2client.BombExploded;

public class Bomb extends GameElement {
	private String id;
	private String playerName;
	
	public Bomb(String playerName, Position position) {
		super(position);
		this.id = UUID.randomUUID().toString();
		this.playerName = playerName;
	}
	
	public List<Player> explodeAndGetKilledPlayers(Map map, PlayerManager playerManager, BombManager bombManager) {
		List<Player> killedPlayers = new ArrayList<Player>();
		
		for (Position position : getPositionsAffectedByExplosion()) {
			map.destructWallAtPosition(position);
			
			Player killedPlayer = playerManager.killPlayerAtPosition(position);
			if (killedPlayer != null) {
				killedPlayers.add(killedPlayer);
			}
		}
		
		Player killer = playerManager.getPlayerOrThrow(playerName);
		killer.addScore(killedPlayers.size());
		
		bombManager.removeBomb(this);
		
		return killedPlayers;
	}
	
	public BombDropped createBombDroppedMessage() {
		return new BombDropped(id, position.getX(), position.getY());
	}
	
	public BombExploded createBombExplodedMessage() {
		return new BombExploded(this.id);
	}
	
	private List<Position> getPositionsAffectedByExplosion() {
		List<Position> positions = new ArrayList<Position>();
		
		positions.add(position);
		positions.add(position.inDirection(Direction.UP));
		positions.add(position.inDirection(Direction.DOWN));
		positions.add(position.inDirection(Direction.LEFT));
		positions.add(position.inDirection(Direction.RIGHT));
		
		return positions;
	}
}
