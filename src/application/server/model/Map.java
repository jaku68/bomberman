package application.server.model;

import java.util.List;

import protocol.server2client.StartGame;
import protocol.server2client.Update;

public class Map {
	private int width, height;
	private List<Position> spawnPoints;
	private List<Wall> walls;
	
	public Map(int width, int height, List<Position> spawnPoints, List<Wall> walls) {
		this.width = width;
		this.height = height;
		this.spawnPoints = spawnPoints;
		this.walls = walls;
	}
	
	public Position getSpawnPoint(int index) {
		return spawnPoints.get(index);
	}
	
	public void destructWallAtPosition(Position position) {
		Wall wallToDestruct = walls.stream()
			.filter(wall -> wall.isAtPosition(position))
			.findFirst().orElse(null);
		
		if (wallToDestruct != null) {
			wallToDestruct.destruct(this);
		}
	}
	
	public void removeWall(Wall wall) {
		walls.remove(wall);
	}
	
	public boolean isOutOfBounds(Position position) {
		boolean left = position.getX() < 0;
		boolean right = position.getX() > width - 1;
		boolean top = position.getY() < 0;
		boolean bottom = position.getY() > height - 1;
		
		return left || right || top || bottom;
	}
	
	public boolean existsAnyWallAtPosition(Position position) {
		return walls.stream()
			.anyMatch(wall -> wall.isAtPosition(position));
	}
	
	public StartGame createStartGameMessage() {
		return new StartGame(getMapDataForClient());
	}
	
	public Update createUpdateMessage() {
		return new Update(getMapDataForClient());
	}
	
	private char[][] getMapDataForClient() {
		char[][] result = new char[height][width];
		
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				Position position = new Position(x, y);
				Wall wall = walls.stream()
					.filter(w -> w.isAtPosition(position))
					.findFirst().orElse(null);
				
				if (wall != null) {
					result[y][x] = wall.getClientRepresentation();
				}
				else {
					result[y][x] = '0';
				}
			}
		}
		
		return result;
	}
}
