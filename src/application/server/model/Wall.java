package application.server.model;

public class Wall extends GameElement {
	private boolean isDestructible;
	
	public Wall(Position position, boolean isDestructible) {
		super(position);
		this.isDestructible = isDestructible;
	}
	
	public void destruct(Map map) {
		if (isDestructible) {
			map.removeWall(this);
		}
	}
	
	public char getClientRepresentation() {
		if (isDestructible) {
			return '1';
		}
		else {
			return '2';
		}
	}
}
