package application.server.model;

import protocol.Direction;
import protocol.server2client.GameOver;
import protocol.server2client.PlayerHit;
import protocol.server2client.PlayerJoined;
import protocol.server2client.PlayerMoved;

public class Player extends GameElement {
	private String name;
	private boolean isAlive = true;
	public int score = 0;
	
	public Player(Position position, String name) {
		super(position);
		this.name = name;
	}
	
	public boolean hasName(String name) {
		return this.name.equals(name);
	}
	
	public boolean isAlive() {
		return isAlive;
	}
	
	public boolean move(Direction direction, Map map, PlayerManager playerManager, BombManager bombManager) {
		Position targetPosition = position.inDirection(direction);
		
		if (!isAlive) {
			return false;
		}
		
		if (willCollide(targetPosition, map, playerManager, bombManager)) {
			return false;
		}
		
		position = targetPosition;
		return true;
	}
	
	public void kill() {
		isAlive = false;
	}
	
	public int getScore() {
		return score;
	}
	
	public void addScore(int score) {
		this.score += score;
	}
	
	public PlayerJoined createPlayerJoinedMessage() {
		return new PlayerJoined(name, position.getX(), position.getY());
	}
	
	public PlayerMoved createPlayerMovedMessage(Direction direction) {
		return new PlayerMoved(name, direction);
	}
	
	public PlayerHit createPlayerHitMessage() {
		return new PlayerHit(name);
	}
	
	public GameOver createGameOverMessage(String[] highScoreList) {
		return new GameOver(name, highScoreList);
	}
	
	public String createHighScoreListItem() {
		return name + ": " + String.valueOf(score);
	}
	
	private static boolean willCollide(Position targetPosition, Map map, PlayerManager playerManager, BombManager bombManager) {
		if (map.isOutOfBounds(targetPosition)) {
			return true;
		}
		
		if (map.existsAnyWallAtPosition(targetPosition)) {
			return true;
		}
		
		if (playerManager.existsAnyPlayerAtPosition(targetPosition)) {
			return true;
		}
		
		if (bombManager.existsAnyBombAtPosition(targetPosition)) {
			return true;
		}
		
		return false;
	}
}
