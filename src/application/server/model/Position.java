package application.server.model;

import protocol.Direction;

public class Position {
	private int x, y;
	
	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public boolean equals(Position position) {
		return x == position.x
			&& y == position.y;
	}
	
	public Position inDirection(Direction direction) {
		switch (direction) {
			case UP:
				return add(0, -1);
			case DOWN:
				return add(0, 1);
			case LEFT:
				return add(-1, 0);
			case RIGHT:
				return add(1, 0);
			default:
				return add(0, 0);
		}
	}
	
	private Position add(int x, int y) {
		return new Position(this.x + x, this.y + y);
	}
}
