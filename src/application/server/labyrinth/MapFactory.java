package application.server.labyrinth;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import application.server.model.Map;
import application.server.model.Position;
import application.server.model.Wall;

public class MapFactory {
	// TODO: Refactor
	public Map create(String path) {
		String fileContent = "";
		try {
			FileReader fr = new FileReader(path);
			BufferedReader br = new BufferedReader(fr);
			String s;
			while((s = br.readLine()) != null) {
				fileContent += s;
				fileContent += System.lineSeparator();
			}
			
			fr.close();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		JSONObject jsonObject = new JSONObject(fileContent);
		JSONArray yArray = (JSONArray)jsonObject.get("map");
		
		int yCounter = 0;
		int xCounter = 0;
		
		List<Position> spawnPoints = new ArrayList<Position>();
		List<Wall> walls = new ArrayList<Wall>();
		
		int width = 0;
		int height = yArray.length();
		
		for (Object y : yArray) {
			JSONArray xArray = (JSONArray)y;
			
			width = xArray.length();
			
			for (Object x : xArray) {
				int value = (int)x;
				
				if (value == -1) {
					spawnPoints.add(new Position(xCounter, yCounter));
				}
				
				if (value == 1) {
					walls.add(new Wall(new Position(xCounter, yCounter), true));
				}
				
				if (value == 2) {
					walls.add(new Wall(new Position(xCounter, yCounter), false));
				}
				
				xCounter++;
			}
			
			yCounter++;
			xCounter = 0;
		}
		
		return new Map(width, height, spawnPoints, walls);
	}
}
