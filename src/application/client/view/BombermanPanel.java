package application.client.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import application.client.control.Control;
import application.client.control.ControlFactory;

@SuppressWarnings("serial")
public class BombermanPanel extends JPanel {
	private JTextField plyerNameTextField = new JTextField();
	private JTextArea messageTextArea = new JTextArea();
	
	
	/**
	 * Panel mit sämtlichen GUI Elementen
	 */
	public BombermanPanel() {
		setLayout(new BorderLayout());
		
		plyerNameTextField.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				// Builderpattern
				Control control = ControlFactory.instance().createJoinGameControl();
			}
		});
		add(plyerNameTextField, BorderLayout.NORTH);
		messageTextArea.setEditable(false);
		messageTextArea.setRows(6);
		add(messageTextArea, BorderLayout.SOUTH);
	}


	public void displayMessage(String message) {
		messageTextArea.append(message + "\n");
		
	}

}
