package application.client.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import application.client.control.PlayerJoinedControl;
import protocol.server2client.PlayerJoined;

import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JScrollBar;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class BombermanFrame extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtServerIp;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BombermanFrame frame = new BombermanFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame
	 */
	public BombermanFrame() {
		setTitle("Bomberman");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTextPane txtpnHighscore = new JTextPane();
		txtpnHighscore.setEditable(false);
		txtpnHighscore.setText("Highscore");
		contentPane.add(txtpnHighscore, BorderLayout.EAST);
		
		JPanel northPanel = new JPanel();
		contentPane.add(northPanel, BorderLayout.NORTH);
		
		txtName = new JTextField();
		txtName.setHorizontalAlignment(SwingConstants.LEFT);
		northPanel.add(txtName);
		txtName.setToolTipText("enter your playername");
		txtName.setText("Playername");
		txtName.setColumns(10);
		
		txtServerIp = new JTextField();
		txtServerIp.setToolTipText("enter the serveradress");
		txtServerIp.setText("Server IP");
		txtServerIp.setHorizontalAlignment(SwingConstants.LEFT);
		txtServerIp.setColumns(10);
		northPanel.add(txtServerIp);
		
		JButton startButton = new JButton("Start");
		startButton.setToolTipText("play the game");
		startButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				System.out.println(txtName.getText());
				
			}
		});
		northPanel.add(startButton);
		
		JPanel mainPanel = new JPanel();
		contentPane.add(mainPanel, BorderLayout.CENTER);
	}

}
