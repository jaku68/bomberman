Vue.component("block", {
    template: `<div>
      <img v-if="type === -1" src="img/bomberman.png" />
      <img v-if="type === 0 || type > 2" src="img/empty.png" />
      <img v-if="type === 1" src="img/box.png" />
      <img v-if="type === 2" src="img/brick.png" />
  </div>`,
    props: ["type"],
  })