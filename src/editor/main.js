
var app = new Vue({
    el: '#app',
    data: {
        output: {
            map: [
                [-1,0,0,1,1,1,1,0,0,-1],
                [0,2,2,2,2,2,2,2,2,0],
                [0,2,2,2,2,2,2,2,2,0],
                [1,2,2,2,2,2,2,2,2,1],
                [1,2,2,2,2,2,2,2,2,1],
                [1,2,2,2,2,2,2,2,2,1],
                [1,2,2,2,2,2,2,2,2,1],
                [0,2,2,2,2,2,2,2,2,0],
                [0,2,2,2,2,2,2,2,2,0],
                [-1,0,0,1,1,1,1,0,0,-1],
            ],
        },
    },
    computed:{
        mapOutput: {
            get: function () {
                return JSON.stringify(this.output);
              },
              // setter
            set: function (newValue) {
                this.output = JSON.parse(newValue);
            }
        },
        downloadData: function(){
            const blob = new Blob([JSON.stringify(this.output)]);
            return URL.createObjectURL(blob);
            
        }
    },
    methods: {
        checkBlock(rowKey, columnKey, value){
            
            // Falls bomberman, blockieren
            if(value === -1){
                return false;
            }

            // Freie Bereiche
            const mapWidth = this.output.map.length-1;
            const emptySpaces = [
                "01",
                "02",
                "10",
                "20",
                (mapWidth-1)+"0",
                (mapWidth-2)+"0",
                mapWidth+"1",
                mapWidth+"2",
                "0"+(mapWidth-1),
                "0"+(mapWidth-2),
                "1"+mapWidth,
                "2"+mapWidth,
                mapWidth+"2",
                mapWidth+""+(mapWidth-1),
                mapWidth+""+(mapWidth-2),
                (mapWidth-1)+""+mapWidth,
                (mapWidth-2)+""+mapWidth,
            ]

            if(emptySpaces.includes(rowKey+""+columnKey)){
                return false;
            }

            // linie zwischen spieler immer zerstörbar
            if(value === 1 && (columnKey === 0 || rowKey === 0 || columnKey === mapWidth || rowKey === mapWidth)){
                return false;
            }
            

        },
        changeBlock(rowKey, columnKey, customValue = false){

            if(this.checkBlock(rowKey, columnKey, this.output.map[rowKey][columnKey]) === false || this.checkBlock(rowKey, columnKey, customValue) === false){
                return;
            }

            let value = customValue ? customValue : this.output.map[rowKey][columnKey] + 1;

            if(value > 2){
                value = 0;
            }

            this.$set(this.output.map[rowKey], columnKey, value );
        },
        random(){
            const map = this.output.map;

            map.forEach((row, rowKey) => {
                map.forEach((column, columnKey) => {
                    this.changeBlock(rowKey, columnKey, Math.floor((Math.random() * 2) + 1));
                });
            });
        },
        increaseSize(){
            const map = this.output.map;
            const currentSize = map.length;
            const middle = Math.round(currentSize / 2);

            if(currentSize > 19){
                return;
            }

            map.splice(middle, 0, [...map[middle]]);

            map.forEach(column => {
                column.splice(middle, 0, column[middle]);
            });
            
            this.random();

        },
        decreaseSize(){
            const map = this.output.map;
            const currentSize = map.length;
            const middle = Math.round(currentSize / 2);

            if(currentSize < 9){
                return;
            }

            map.splice(middle, 1);

            map.forEach(column => {
                column.splice(middle, 1);
            });

            this.random();
        },
        fileUpload(event){
            
            var reader = new FileReader();

            reader.onload = (event) => {
                const FileData = JSON.parse(event.target.result);
                this.output = FileData;
            }

            reader.readAsText(event.target.files[0]);

        }
    },
    mounted(){
        this.random();
    }
});

