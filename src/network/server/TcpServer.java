package network.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.Map;
import java.util.HashMap;

import network.Message;

public class TcpServer extends Server {
    private ServerSocket socket;
    private Map<String, Socket> clientSockets = new HashMap<String, Socket>();

    /**
     * Konstruktor. Initialisiert das neue Server-Objekt mit der Referenz auf das Empf�ngerobjekt.
     *
     * @param serverApplication Das Empf�ngerobjekt des Bomberman-Servers f�r Nachrichten.
     */
    public TcpServer(ServerApplicationInterface serverApplication) {
        super(serverApplication);
    }

    public void connect() {
        try {
            socket = new ServerSocket(5420);
            final Socket client = socket.accept();
            handleClientSocket(client);
        } catch (Exception error) {
            System.out.println("OHNOES");
        }
    }

    private static void handleClientSocket(Socket client) {
    	new Thread(new SocketHandler(client)).start();
    }


    private static void readFullRequest(Socket client) throws IOException {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(client.getInputStream()));
        String line = reader.readLine();
        while (line != null && !line.isEmpty()) {
            line = reader.readLine();
        }
    }

    @Override
    public void send(Message message, String connectionId) {
    	Socket client = clientSockets.get(connectionId);
    	
    	if(client == null) {
    		//TODO there is no socket
    		return;
    	}
    	
    	//TODO send msg
    }

    @Override
    public void broadcast(Message message) {
    	for(Socket s : clientSockets.values()) {
    		//TODO send to every socket
    	}
    }
}
