package network.client;

import io.reactivex.*;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.subscriptions.AsyncSubscription;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import network.Message;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.concurrent.Future;

import static java.net.SocketOptions.TCP_NODELAY;

public class TcpServerProxy extends ServerProxy {
    Subject<Message> sender;
    PublishSubject<Observable<ByteBuffer>> byteReceiver;
    Observable<Message> receiver;
    Observable<AsynchronousSocketChannel> channel;

    Disposable connectionSubscription;
    Disposable messageSubscription;

    /**
     * Konstruktor. Initialisiert das neue ServerProxy-Objekt mit der Referenz auf das Empf�ngerobjekt.
     *
     * @param clientApplication Das Empf�ngerobjekt des Bomberman-Clients f�r Nachrichten.
     */
    public TcpServerProxy(ClientApplicationInterface clientApplication) {
        super(clientApplication);

        sender = PublishSubject.create();
        receiver = byteReceiver
                .concatMap(obs -> obs)
                .map(buffer -> deserializeMessage(buffer));
    }

    @Override
    public void connect(String host, int port) {
        FlowableOnSubscribe<AsynchronousSocketChannel> handler = observer -> {
            AsynchronousSocketChannel client = AsynchronousSocketChannel.open();
            InetSocketAddress hostAddress = new InetSocketAddress("localhost", 6969);
            Future<Void> future = client.connect(hostAddress);

            observer.onNext(client);

            observer.setCancellable(() -> {
                future.cancel(false);
                client.close();
            });
        };

        connectionSubscription = Flowable.combineLatest(
                Flowable.create(handler, BackpressureStrategy.BUFFER)
                        .subscribeOn(Schedulers.io())
                        .doOnNext(channel -> {
                            ByteBuffer buffer = ByteBuffer.allocate(10240);
                            byteReceiver.onNext(Observable.fromFuture(channel.read(buffer))
                                    .map(result -> buffer));
                        }),
                sender.toFlowable(BackpressureStrategy.DROP),
                (channel, message) ->
                        channel.write(serializeMessage(message))
        ).subscribe();

        messageSubscription = receiver
                .subscribe(message -> clientApplication.handleMessage(message));
    }

    @Override
    public void disconnect() {
        connectionSubscription.dispose();
        messageSubscription.dispose();
    }

    @Override
    public void send(Message message) {
        sender.onNext(message);
    }

    private ByteBuffer serializeMessage(Message message) {
        return ByteBuffer.allocate(0);
    }

    private Message deserializeMessage(ByteBuffer bytes) {
        return null;
    }
}
